# API and Signature keys sharing via post message

To request users API and Signature keys you need to make GET request to

```
https://accounts.hodlhodl.com/accounts/request_access?attributes=api_key,api_signature_key
```

User will be asked if he allows to share his keys to the current application.

If user allowed access, the page will leave post message with API and Signature keys:

```json
{
  "allowed": true,
  "data": {
    "api_key": "<api_key>",
    "api_signature_key": "<signature_key",
  }
}
```

If user didn't allow access, the page will leave post message with corresponding flag:
```json
{
  "allowed": false
}
```
