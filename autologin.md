# Autologin

Users can automatically get logged in by providing a token in URL parameters.
This document describes how to build an autologin link using HodlHodl API.

## Issue an autologin token

To build a vaild request for issuing a token user must sign it with HMAC-SHA256 algorithm using it's **signature key**,
which can be found on API preferences page.

Source message for signing must be in the next format:
```
<api_key>:<nonce>
```

* `api_key` - is the user's API key that is used for authorization
* `nonce` - current unix timestamp (must not be older than 30 seconds)

### Signature example

```
API key:
iqZC7uUmx4sVeIwFQN2YqGT5SyrXNLhxVX7QMGUeJK1CDdy87OcrOt3QvPE5LFC56Lgu7WLlg12U55Vy

Nonce:
1589980224

Message:
iqZC7uUmx4sVeIwFQN2YqGT5SyrXNLhxVX7QMGUeJK1CDdy87OcrOt3QvPE5LFC56Lgu7WLlg12U55Vy:1589980224

Signature key:
cce14197a08ebab7cfbb41cfce9fe91e0f31d572d3f48571ca3c30bfd516f769

Signature:
1d2a51ca2c54ff9107a3460b22f01bc877e527a9a719d81b32038741332159fc
```

### Token request

The request must include API key in `Authorization` header as usual API request.

Two additional parameters required for request: `nonce` and `hmac` (signature).

```
https://hodlhodl.com/api/v1/users/login_token?nonce=1589980224&hmac=1d2a51ca2c54ff9107a3460b22f01bc877e527a9a719d81b32038741332159fc
```

The server will respond with result object:
```json
{
  "success": true,
  "token": "1234567890"
}
```

If nonce is older than 30 seconds or user already made request with this nonce
the server will respond with 403 status and error code:
```json
{
  "success": false,
  "error_code": "nonce_expired"
}
```

If request signature is not valid
the server will respond with 403 status and error code:
```json
{
  "success": false,
  "error_code": "signature_invalid"
}
```

## Build autologin URL

Add to URL you want to open additional parameter `sign_in_token`.

```
https://hodlhodl.com/contracts?sign_in_token=1234567890
```

Then user will be redirected to the provided URL being automatically logged in.

Note that if user has 2fa enabled, it will be asked for 2fa code.
